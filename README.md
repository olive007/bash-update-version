# Script to update the version of the repository.
The type of update have to be one of the 3 following types:
- Major: The first number will be increase of 1 and the 2 other will be set to 0
- Minor: The second number will be increase of 1 and the third number will be set to 0
- Patch: The third number will be increase of 1

The script will switch to the master branch and commit the version once it is updated.

##  How to use
Just set the 2 following variable into the script.
````bash
VERSION_FILE_NAME="version.txt"
PACKAGE_JSON_FILE_NAMES=("package.json" "custom/package.json")
````
