#!/usr/bin/env bash

# Global variable set by the function mocking_exit to allow us to check the error output of the command.
ERROR_MESSAGE=""

# Exit the test if the 2 argument are not equals.
assert_equal() {
  if [[ "$1" != "$2" ]]; then
    echo -e "Error: '$2' is expected but '$1' has been receive" >&2
    exit 2
  fi
}

# Mock the bash builtin 'exit'. This function will set the global variable 'ERROR_MESSAGE' with the error output.
mocking_exit() {
  local return_code_expected=$1
  shift

  # Save the error output into the global variable
  ERROR_MESSAGE=$(bash -c "$(declare -f); $*" 2>&1 >/dev/null)
  return_code=$?

  # Check if the return code is correct.
  if [[ "$return_code" != "$return_code_expected" ]]; then
    echo "Error: Exit code $return_code_expected is expected but $return_code has been receive"
    exit 2
  fi
}

test_parse_args() {
  source update_version.sh

	# no parameter
  mocking_exit 2 parse_args
  assert_equal "$ERROR_MESSAGE" "Error: Illegal number of parameters"

  # Illegal option
  mocking_exit 2 parse_args -s minor
  assert_equal "$ERROR_MESSAGE" "bash: illegal option -- s"

  # Wrong type of update
  mocking_exit 2 parse_args main
  assert_equal "$ERROR_MESSAGE" "Error: Unsupported type of update"

  # right command (usage)
  mocking_exit 0 parse_args -h

  # right command
  parse_args minor
  assert_equal "$DRY_RUN" "false"
  assert_equal "$update_type" "minor"

  parse_args -d minor
  assert_equal "$DRY_RUN" "true"
  assert_equal "$update_type" "minor"

  parse_args minor -d
  assert_equal "$DRY_RUN" "true"
  assert_equal "$update_type" "minor"
}

# Loop from this file all functions starting with 'test_' ...
IFS=$'\n'
for f in $(declare -F | grep "test_"); do
  # ... to run them individually.
  test_to_run="${f:11}"
  echo "Running bash unit test: ${test_to_run:5}"

  # Save the result of the command ...
  result=$(bash -c "$(declare -f); $test_to_run" 2>&1)
  return_code=$?
  if [[ $return_code -ne 0 ]]; then
    # ... and display it if the test failed.
    echo "--- FAILURE in test ${test_to_run:5} ---" >&2
    echo "$result"
    exit 1
  fi
done