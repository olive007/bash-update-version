#!/usr/bin/env bash

VERSION_FILE_NAME="version.txt"
PYPI_SETUP_FILE_NAMES=("setup.py" "setup_version_last.py")
PACKAGE_JSON_FILE_NAMES=("package.json" "package_version_last.json")
DRY_RUN=false

readonly OLD_VERSION=$(cat $VERSION_FILE_NAME)
readonly COMMAND_NAME=$0

usage() {
	cat <<-EOF
		
			Script to update the version. The update can be one of the 3 types:
			 - Major: The first number will be increase of 1 and the 2 other will be set to 0
			 - Minor: The second number will be increase of 1 and the third number will be set to 0
			 - Patch: The third number will be increase of 1
		
			====== Example =====
		
				$COMMAND_NAME  major|minor|patch
		
			==== Option List ===
		
				One argument is always required. It have to be major, minor or patch. The case is insensitive.
		
				-h: Display this help message.
		
			====================
	EOF
}

parse_args() {
	local OPTIND # Without this variable declaration the function will have a incorrect behaviour on future calls
	local option
	local arguments=("$@")
	local argument_number=$#
	local tmp=""

	if [ "$#" -ge 1 ]; then
		if ! [[ "$1" =~ ^-.* ]]; then
			tmp=$1
			shift
		fi
	fi

	# Parse the arguments with the bash builtin 'getopts'
	while getopts "hd" option; do
		case ${option} in
		'd')
			DRY_RUN=true
			continue
			;;
		'h')
			usage
			exit 0
			;;
		'?')
			exit 2
			;;
		esac
	done

	if [ "$((argument_number - OPTIND))" -ne 0 ]; then
		echo 1>&2 "Error: Illegal number of parameters"
		exit 2
	fi
	if [ -z "$tmp" ]; then
		update_type=$(echo "${!OPTIND}" | tr '[:upper:]' '[:lower:]')
	else
		update_type=$(echo "$tmp" | tr '[:upper:]' '[:lower:]')
	fi
	if [[ $update_type != "major" && $update_type != "minor" && $update_type != "patch" ]]; then
		echo 1>&2 "Error: Unsupported type of update"
		exit 2
	fi
}

update_version() {
	default_ifs="$IFS"
	IFS='.'
	read -r -a numbers < <(echo "$OLD_VERSION")

	if [[ $update_type == "major" ]]; then
		numbers[0]=$((numbers[0] + 1))
		numbers[1]=0
		numbers[2]=0
		new_version="${numbers[*]}"
	elif [[ $update_type == "minor" ]]; then
		numbers[1]=$((numbers[1] + 1))
		numbers[2]=0
		new_version="${numbers[*]}"
	else
		numbers[2]=$((numbers[2] + 1))
		new_version="${numbers[*]}"
	fi
	IFS="$default_ifs"

	echo "Updating the version from $OLD_VERSION to $new_version"

	if [[ -d ".git" ]]; then
		commit_new_version
	else
		update_file
	fi
}

commit_new_version() {
	set -e
	git_options=""
	if $DRY_RUN; then
		git_options="--dry-run"
	fi

	git_status=$(git status --untracked-files=no --porcelain)
	if [ -z "$git_status" ]; then
		branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
		branch_name="(unnamed branch)"     # detached HEAD
		branch_name=${branch_name##refs/heads/}

		if [[ "$branch_name" != "master" ]] && [[ ! $DRY_RUN ]]; then
			git checkout master
			git rebase "$branch_name"
		fi
		update_file

		git add $VERSION_FILE_NAME "${PACKAGE_JSON_FILE_NAMES[@]}" "${PYPI_SETUP_FILE_NAMES[@]}" $git_options
		git commit -m "Version $new_version" $git_options
		git tag v$new_version -m "version $new_version" $git_options

		if [[ "$branch_name" != "master" ]]; then
			git checkout "$branch_name"
			git rebase master
		fi
	else
		echo 1>&2 "Errors: Your git repository isn't clean"
		echo 1>&2 "$git_status"
		exit 2
	fi
}

update_file() {
	if ! $DRY_RUN; then
		echo "$new_version" > "$VERSION_FILE_NAME"
		for file_name in "${PYPI_SETUP_FILE_NAMES[@]}"; do
			sed -i -e "s/version=\".*\"/version=\"$new_version\"/g" "$file_name"
		done
		for file_name in "${PACKAGE_JSON_FILE_NAMES[@]}"; do
			sed -i -e "s/\"version\": \".*\"/\"version\": \"$new_version\"/g" "$file_name"
		done
	fi
}

# Check if we sourced the file or try to run it.
if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
	# The file is not sourced so we can execute the code.
	parse_args "$@"
	update_version
fi
